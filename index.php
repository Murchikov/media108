<!DOCTYPE html>
<html>
	<head>
		<title>Тестовое задание для Media 108</title>
		<script src="/js/jquery.min.js"></script>
		<script>
			$(document).ready(function () {
			
				//обработка формы
				$("#form").on("submit", (function(e) {
					e.preventDefault();
					$.ajax({
						type: "POST",
						url: "/compute.php",
						data: new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function (data) {
							$("#c").val(data);
							$("#div1").css("display", "block");
						},
						error: function()
						{
							alert("Возникла ошибка");
						}
					});
				}));
				
				//обработка сравнения
				$("#compare").on("input", function() {
					var c = parseInt($("#c").val(), 10);
					var compare = parseInt($("#compare").val(), 10);
					
					if(c > compare)
					{
						$("#c").css("color", "red");
						$("#c").css("border", "1px solid red");
						$("#c").css("font-size", "120%");
						
						//добавление html-элементов
						$('#result').empty();
						$('#result').append('<span style="color: red;"><b>Результат вычислений больше введенного числа</b></span>');
					}
					if(c < compare)
					{
						$("#c").css("color", "green");
						$("#c").css("border", "1px solid green");
						$("#c").css("font-size", "100%");
						
						//добавление html-элементов
						$('#result').empty();
						$('#result').append('<span style="color: green;"><b>Результат вычислений меньше введенного числа</b></span>');
					}
					if(c == compare)
					{
						$("#c").css("font-style", "normal");
						$("#c").css("font-weight", "normal");
						$("#c").css("color", "");
						$("#c").css("border", "");
						
						//добавление html-элементов
						$('#result').empty();
						$('#result').append('<span style="color: black;"><b>Результат вычислений равен введенному числу</b></span>');
					}
					if(c != compare)
					{
						$("#c").addClass("cursive");
					}
				});
			});				
		</script>
		<style type="text/css">
			.cursive {
				font-weight: bold;
				font-style: italic;
			}
		</style>
	</head>
	
	<body>
		<form id="form" method="POST" action="javascript:void(null);">
			<p>Введите число <b>a</b>: <input id="a" type="text" name="a" /></p>
			<p>Введите число <b>b</b>: <input id="b" type="text" name="b" /></p>
			<p>Введите шаблон действия над переменными <b>a</b> и <b>b</b>: <input type="text" name="pattern" /></p>
			<p><input type="submit" value="Расчитать" /></p>
		</form>
		<p>Результат расчетов: <input id="c" type="text" disabled/></p>		

		<!-- ввод условия и числа, с которым сравнить результат расчетов "c" -->
		<div id="div1" style="display: none;">
			<p>Введите число для сравнения с результатом: <input id="compare" type="text" /></p>
		</div>
		
		<!-- Сообщение результата -->
		<div id="result">
		</div>
	</body>
</html>